package pl.koobe24;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EpipleuraBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(EpipleuraBootApplication.class, args);
	}
}
