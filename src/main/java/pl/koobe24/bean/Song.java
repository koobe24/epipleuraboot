package pl.koobe24.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by tomasz on 24.12.16.
 */
@NamedNativeQueries({
        @NamedNativeQuery(
                name = "getTenWithIndex",
                query = "CALL getTenWithIndex(:index)",
                resultClass = Song.class
        ),
        @NamedNativeQuery(
                name = "getBySongNameStartingWith",
                query = "CALL getBySongNameStartingWith(:name)",
                resultClass = Song.class
        ),
        @NamedNativeQuery(
                name = "getOrderedByOverallLoudness",
                query = "CALL getOrderedByOverallLoudness(:ascending)",
                resultClass = Song.class
        ),
        @NamedNativeQuery(name = "getWithMinimalAndMaximalDuration",
                query = "CALL getWithMinimalAndMaximalDuration(:min, :max)",
                resultClass = Song.class),
        @NamedNativeQuery(name = "getByAlbumSortedByDuration",
                query = "CALL getByAlbumSortedByDuration(:albumName,:ascending)",
                resultClass = Song.class),
        @NamedNativeQuery(name = "getSongsWithArtistAndPublisher",
                query = "CALL getSongsWithArtistAndPublisher(:p_artist,:p_publisher)",
                resultClass = Song.class),
        @NamedNativeQuery(name = "getFromEvenOrUnevenYears",
                query = "CALL getFromEvenOrUnevenYears(:even)",
                resultClass = Song.class),
        @NamedNativeQuery(name = "getLongestAndShortestSongs",
                query = "CALL getLongestAndShortestSongs(:howMany)",
                resultClass = Song.class),
        @NamedNativeQuery(name = "getSongsWithTag",
                query = "CALL getSongsWithTag(:tag)",
                resultClass = Song.class),
        @NamedNativeQuery(name = "getRandomSongsFromArtist",
                query = "CALL getRandomSongsFromArtist(:howMany,:p_artist)",
                resultClass = Song.class)

})
@XmlRootElement(name = "song")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Song {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String title;
    private String comments;
    private String artist;
    private String feat;
    private String album;
    private int year;
    private int albumOrder;
    private Genre genre;
    private int duration;
    private int sampleRate;
    private int BPM;
    private String publisher;
    private String tags;
    private int overallLoudness;
    private int maximumSegmentLoudness;

    public enum Genre {
        RAP, ROCK, METAL, POP, DISC_POLO;

        @Override
        public String toString() {
            String helper = super.toString().toLowerCase();
            return helper.substring(0, 1).toUpperCase() + helper.substring(1);
        }
    }
}

