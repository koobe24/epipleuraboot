package pl.koobe24.bean;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by tomasz on 24.12.16.
 */
@XmlRootElement
@XmlSeeAlso({Song.class})
public class SongList {

    @XmlAnyElement
    private Collection<Song> songList;

    public SongList(){
        songList=new ArrayList<Song>();
    }

    public SongList(Collection<Song> songList){
        this.songList=songList;
    }

    public Collection<Song> getSongList() {
        return songList;
    }
}
