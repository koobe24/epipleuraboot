package pl.koobe24.bean;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by tomas on 28.12.2016.
 */
@Entity(name = "user_css")
@Data
@NamedNativeQueries({
        @NamedNativeQuery(name = "getLastCssForIp",
                query = "CALL getLastCssForIp(:p_ip)",
                resultClass = UserCss.class)
})
public class UserCss {
    @Id
    @GeneratedValue
    private int id;
    private String ip;
    @Column(length = 5000)
    private String css;
}
