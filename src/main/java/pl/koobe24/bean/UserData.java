package pl.koobe24.bean;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by tomasz on 24.12.16.
 */
@Entity(name = "user_data")
@Data
public class UserData {
    @Id
    @GeneratedValue
    private int id;
    private String ipAddress;
    private Date dateTime;
    private String userAgent;
    private String view;
}
