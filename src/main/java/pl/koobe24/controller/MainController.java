package pl.koobe24.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.koobe24.repository.SongRepository;

import java.util.Collection;

/**
 * Created by tomasz on 26.12.16.
 */
@Controller
@RequestMapping("/")
public class MainController {

    private SongRepository songRepository;

    @Autowired
    public void setSongRepository(SongRepository songRepository) {
        this.songRepository = songRepository;
    }

    @RequestMapping
    public String main(Model model) {
        Collection<String> albums = songRepository.getAlbums();
        Collection<String> artists = songRepository.getArtists();
        Collection<String> publishers = songRepository.getPublishers();
        model.addAttribute("albums", albums);
        model.addAttribute("publishers", publishers);
        model.addAttribute("artists", artists);
        return "home";
    }
}
