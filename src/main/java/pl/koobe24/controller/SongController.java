package pl.koobe24.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.koobe24.repository.SongRepository;

/**
 * Created by tomasz on 25.12.16.
 */
public abstract class SongController {

    protected SongRepository songRepository;

    @Autowired
    protected void setSongRepository(SongRepository songRepository) {
        this.songRepository = songRepository;
    }

    @RequestMapping("/showSongsByName")
    public abstract String showSongsByName(String name, Model model);

    @RequestMapping("/getTenWithIndex")
    public abstract String getTenWithIndex(int index, Model model);

    @RequestMapping("/getOrderedByOverallLoudness")
    public abstract String getOrderedByOverallLoudness(String ascending, Model model);

    @RequestMapping("/getByAlbumSortedByDuration")
    public abstract String getByAlbumSortedByDuration(String name, String ascending, Model model);

    @RequestMapping("/getWithMinimalAndMaximalDuration")
    public abstract String getWithMinimalAndMaximalDuration(int min, int max, Model model);

    @RequestMapping("/getSongsWithArtistAndPublisher")
    public abstract String getSongsWithArtistAndPublisher(String artist, String publisher, Model model);

    @RequestMapping("/getFromEvenOrUnevenYears")
    public abstract String getFromEvenOrUnevenYears(boolean even, Model model);

    @RequestMapping("/getLongestAndShortestSongs")
    public abstract String getLongestAndShortestSongs(int howMany, Model model);

    @RequestMapping("/getSongsWithTag")
    public abstract String getSongsWithTag(String tag, Model model);

    @RequestMapping("/getRandomSongsFromArtist")
    public abstract String getRandomSongsFromArtist(int howMany, String artist, Model model);
}
