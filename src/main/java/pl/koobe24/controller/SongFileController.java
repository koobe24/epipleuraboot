package pl.koobe24.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.koobe24.bean.Song;
import pl.koobe24.repository.SongRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;

/**
 * Created by tomasz on 26.12.16.
 */
@Slf4j
public abstract class SongFileController {

    private SongRepository songRepository;

    @Autowired
    protected void setSongRepository(SongRepository songRepository) {
        this.songRepository = songRepository;
    }

    @RequestMapping("/showSongsByName")
    public void showSongsByName(String name, HttpServletRequest request, HttpServletResponse response) {
        Collection<Song> songs = songRepository.getBySongNameStartingWith(name);
        File file = writeToFile(songs, request);
        addFileToResponse("name_starting_with" + getFileExtension(), file, response);
        if (file.delete()) {
            log.info("Deleted temp file.");
        } else {
            log.info("Couldn't delete temp file");
        }
    }

    @RequestMapping("/getTenWithIndex")
    public void getTenWithIndex(int index, HttpServletRequest request, HttpServletResponse response) {
        Collection<Song> songs = songRepository.getTenWithIndex(index);
        File file = writeToFile(songs, request);
        addFileToResponse("ten_with_index" + getFileExtension(), file, response);
        if (file.delete()) {
            log.info("Deleted temp file.");
        } else {
            log.info("Couldn't delete temp file");
        }
    }

    @RequestMapping("/getOrderedByOverallLoudness")
    public void getOrderedByOverallLoudness(String ascending, HttpServletRequest request, HttpServletResponse response) {
        int asc = ascending != null ? 1 : -1;
        Collection<Song> songs = songRepository.getOrderedByOverallLoudness(asc);
        File file = writeToFile(songs, request);
        addFileToResponse("by_overall_loudness" + getFileExtension(), file, response);
        if (file.delete()) {
            log.info("Deleted temp file.");
        } else {
            log.info("Couldn't delete temp file");
        }
    }

    @RequestMapping("/getByAlbumSortedByDuration")
    public void getByAlbumSortedByDuration(String name, String ascending, HttpServletRequest request, HttpServletResponse response) {
        int asc = ascending != null ? 1 : -1;
        Collection<Song> songs = songRepository.getByAlbumSortedByDuration(name, asc);
        File file = writeToFile(songs, request);
        addFileToResponse("sorted_by_duration" + getFileExtension(), file, response);
        if (file.delete()) {
            log.info("Deleted temp file.");
        } else {
            log.info("Couldn't delete temp file");
        }
    }

    @RequestMapping("/getWithMinimalAndMaximalDuration")
    public void getWithMinimalAndMaximalDuration(int min, int max, HttpServletRequest request, HttpServletResponse response) {
        if (max == 0) {
            max = Integer.MAX_VALUE;
        }
        Collection<Song> songs = songRepository.getWithMinimalAndMaximalDuration(min, max);
        File file = writeToFile(songs, request);
        addFileToResponse("with_min_max_duration" + getFileExtension(), file, response);
        if (file.delete()) {
            log.info("Deleted temp file.");
        } else {
            log.info("Couldn't delete temp file");
        }
    }

    @RequestMapping("/getSongsWithArtistAndPublisher")
    public void getSongsWithArtistAndPublisher(String artist, String publisher, HttpServletRequest request, HttpServletResponse response) {
        Collection<Song> songs = songRepository.getSongsWithArtistAndPublisher(artist, publisher);
        File file = writeToFile(songs, request);
        addFileToResponse("with_artist_and_publisher" + getFileExtension(), file, response);
        if (file.delete()) {
            log.info("Deleted temp file.");
        } else {
            log.info("Couldn't delete temp file");
        }
    }

    @RequestMapping("/getFromEvenOrUnevenYears")
    public void getFromEvenOrUnevenYears(boolean even, HttpServletRequest request, HttpServletResponse response) {
        Collection<Song> songs = songRepository.getFromEvenOrUnevenYears(even);
        File file = writeToFile(songs, request);
        addFileToResponse("even_uneven_years" + getFileExtension(), file, response);
        if (file.delete()) {
            log.info("Deleted temp file.");
        } else {
            log.info("Couldn't delete temp file");
        }
    }

    @RequestMapping("/getLongestAndShortestSongs")
    public void getLongestAndShortestSongs(int howMany, HttpServletRequest request, HttpServletResponse response) {
        Collection<Song> songs = songRepository.getLongestAndShortestSongs(howMany);
        File file = writeToFile(songs, request);
        addFileToResponse("longest_shortest" + getFileExtension(), file, response);
        if (file.delete()) {
            log.info("Deleted temp file.");
        } else {
            log.info("Couldn't delete temp file");
        }
    }

    @RequestMapping("/getSongsWithTag")
    public void getSongsWithTag(String tag, HttpServletRequest request, HttpServletResponse response) {
        Collection<Song> songs = songRepository.getSongsWithTag(tag);
        File file = writeToFile(songs, request);
        addFileToResponse("with_tag" + getFileExtension(), file, response);
        if (file.delete()) {
            log.info("Deleted temp file.");
        } else {
            log.info("Couldn't delete temp file");
        }
    }

    @RequestMapping("/getRandomSongsFromArtist")
    public void getRandomSongsFromArtist(int howMany, String artist, HttpServletRequest request, HttpServletResponse response) {
        Collection<Song> songs = songRepository.getRandomSongsFromArtist(howMany, artist);
        File file = writeToFile(songs, request);
        addFileToResponse("random_from_artist" + getFileExtension(), file, response);
        if (file.delete()) {
            log.info("Deleted temp file.");
        } else {
            log.info("Couldn't delete temp file");
        }
    }

    protected abstract File writeToFile(Collection<Song> songs, HttpServletRequest request);

    protected abstract String getFileExtension();

    private void addFileToResponse(String fileName, File file, HttpServletResponse response) {
        try (FileInputStream in = new FileInputStream(file)) {
            response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
            FileCopyUtils.copy(in, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            e.printStackTrace();
            log.info("Adding file " + fileName + " to response failed.");
        }
    }
}
