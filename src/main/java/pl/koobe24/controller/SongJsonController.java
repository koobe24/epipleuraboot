package pl.koobe24.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.koobe24.bean.Song;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

/**
 * Created by tomasz on 26.12.16.
 */
@Controller
@RequestMapping("/json")
public class SongJsonController extends SongFileController {
    @Override
    protected File writeToFile(Collection<Song> songs, HttpServletRequest request) {
        ObjectMapper converter = new ObjectMapper();
        File file = new File(System.currentTimeMillis() + getFileExtension());
        try (FileWriter writer = new FileWriter(file)) {
            writer.write(converter.writeValueAsString(songs));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    @Override
    protected String getFileExtension() {
        return ".json";
    }
}
