package pl.koobe24.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.koobe24.bean.Song;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

import static pl.koobe24.controller.util.OgdlCreator.collectionToOgdlString;

/**
 * Created by tomasz on 27.12.16.
 */
@Controller
@RequestMapping("/ogdl")
public class SongOgdlController extends SongFileController {
    @Override
    protected File writeToFile(Collection<Song> songs, HttpServletRequest request) {
        File file = new File(System.currentTimeMillis() + getFileExtension());
        try (FileWriter writer = new FileWriter(file)) {
            writer.write(collectionToOgdlString(songs));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    @Override
    protected String getFileExtension() {
        return ".ogdl";
    }
}
