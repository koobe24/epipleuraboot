package pl.koobe24.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.koobe24.bean.Song;

import java.util.Collection;

/**
 * Created by tomasz on 24.12.16.
 */
@Controller
@RequestMapping("/view")
@Slf4j
public class SongViewController extends SongController {

    @Override
    @RequestMapping("/showSongsByName")
    public String showSongsByName(@RequestParam("name") String name, Model model) {
        Collection<Song> songs = songRepository.getBySongNameStartingWith(name);
        model.addAttribute("songs", songs);
        model.addAttribute("name", name);
        return "songs";
    }

    @Override
    @RequestMapping("/getTenWithIndex")
    public String getTenWithIndex(@RequestParam("index") int index, Model model) {
        Collection<Song> songs = songRepository.getTenWithIndex(index);
        model.addAttribute("songs", songs);
        model.addAttribute("index", index);
        return "songs";
    }

    @Override
    @RequestMapping("/getOrderedByOverallLoudness")
    public String getOrderedByOverallLoudness(@RequestParam(required = false, value = "ascending") String ascending, Model model) {
        int asc = ascending != null ? 1 : -1;
        Collection<Song> songs = songRepository.getOrderedByOverallLoudness(asc);
        model.addAttribute("songs", songs);
        model.addAttribute("ascending", ascending);
        return "songs";
    }

    @Override
    @RequestMapping("/getByAlbumSortedByDuration")
    public String getByAlbumSortedByDuration(@RequestParam("name") String name, @RequestParam(required = false, value = "ascending") String ascending, Model model) {
        int asc = ascending != null ? 1 : -1;
        Collection<Song> songs = songRepository.getByAlbumSortedByDuration(name, asc);
        model.addAttribute("songs", songs);
        model.addAttribute("name", name);
        model.addAttribute("ascending", ascending);
        return "songs";
    }

    @Override
    @RequestMapping("/getWithMinimalAndMaximalDuration")
    public String getWithMinimalAndMaximalDuration(@RequestParam(required = false, value = "min") int min, @RequestParam(required = false, value = "max") int max, Model model) {
        if (max == 0) {
            max = Integer.MAX_VALUE;
        }
        Collection<Song> songs = songRepository.getWithMinimalAndMaximalDuration(min, max);
        model.addAttribute("songs", songs);
        model.addAttribute("min", min);
        model.addAttribute("max", max);
        return "songs";
    }

    @Override
    @RequestMapping("/getSongsWithArtistAndPublisher")
    public String getSongsWithArtistAndPublisher(@RequestParam("artist") String artist, @RequestParam("publisher") String publisher, Model model) {
        Collection<Song> songs = songRepository.getSongsWithArtistAndPublisher(artist, publisher);
        model.addAttribute("songs", songs);
        model.addAttribute("artist", artist);
        model.addAttribute("publisher", publisher);
        return "songs";
    }

    @Override
    @RequestMapping("/getFromEvenOrUnevenYears")
    public String getFromEvenOrUnevenYears(@RequestParam("even") boolean even, Model model) {
        Collection<Song> songs = songRepository.getFromEvenOrUnevenYears(even);
        model.addAttribute("songs", songs);
        model.addAttribute("even", even);
        return "songs";
    }

    @Override
    @RequestMapping("/getLongestAndShortestSongs")
    public String getLongestAndShortestSongs(@RequestParam("howMany") int howMany, Model model) {
        Collection<Song> songs = songRepository.getLongestAndShortestSongs(howMany);
        model.addAttribute("songs", songs);
        model.addAttribute("howMany", howMany);
        return "songs";
    }

    @Override
    @RequestMapping("/getSongsWithTag")
    public String getSongsWithTag(@RequestParam("tag") String tag, Model model) {
        Collection<Song> songs = songRepository.getSongsWithTag(tag);
        model.addAttribute("songs", songs);
        model.addAttribute("tag", tag);
        return "songs";
    }

    @Override
    @RequestMapping("/getRandomSongsFromArtist")
    public String getRandomSongsFromArtist(@RequestParam("howMany") int howMany, @RequestParam("artist") String artist, Model model) {
        Collection<Song> songs = songRepository.getRandomSongsFromArtist(howMany, artist);
        model.addAttribute("songs", songs);
        model.addAttribute("artist", artist);
        model.addAttribute("howMany", howMany);
        return "songs";
    }
}
