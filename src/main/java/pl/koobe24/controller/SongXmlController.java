package pl.koobe24.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.koobe24.bean.Song;
import pl.koobe24.bean.SongList;
import pl.koobe24.bean.UserCss;
import pl.koobe24.repository.UserCssRepository;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by tomasz on 25.12.16.
 */
@Controller
@RequestMapping("/xml")
@Slf4j
public class SongXmlController extends SongFileController {

    private UserCssRepository userCssRepository;

    @Autowired
    public void setUserCssRepository(UserCssRepository userCssRepository) {
        this.userCssRepository = userCssRepository;
    }

    @Override
    protected File writeToFile(Collection<Song> songs, HttpServletRequest request) {
        File xmlFile = new File(System.currentTimeMillis() + getFileExtension());
        try (FileWriter writer = new FileWriter(xmlFile)) {
            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
            writer.write("<?xml-stylesheet href=\"style.css\"?>");
        } catch (IOException e) {
            e.printStackTrace();
        }
        SongList songList = new SongList(songs);
        try (FileWriter appender = new FileWriter(xmlFile, true)) {
            JAXBContext jaxbContext = JAXBContext.newInstance(SongList.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
            jaxbMarshaller.marshal(songList, appender);
        } catch (JAXBException | IOException e) {
            e.printStackTrace();
        }
        File cssFile = createCssFile(request);
        File outputFile = outputZipFile(xmlFile, cssFile);
        if (xmlFile.delete()) {
            log.info("Deleted unused xml file.");
        } else {
            log.info("Couldn't delete xml file.");
        }
        if (cssFile.delete()) {
            log.info("Deleted unused css file.");
        } else {
            log.info("Couldn't delete css file.");
        }
        return outputFile;
    }

    private File outputZipFile(File xmlFile, File cssFile) {
        File outputFile = new File(System.currentTimeMillis() + getFileExtension());
        try (ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(outputFile));
             FileInputStream xmlIn = new FileInputStream(xmlFile);
             FileInputStream cssIn = new FileInputStream(cssFile)) {
            ZipEntry xmlEntry = new ZipEntry("data.xml");
            zipOutputStream.putNextEntry(xmlEntry);
            byte[] buffer = new byte[1024];
            for (int count; (count = xmlIn.read(buffer, 0, buffer.length)) != -1; ) {
                zipOutputStream.write(buffer, 0, count);
            }
            ZipEntry cssEntry = new ZipEntry("style.css");
            zipOutputStream.putNextEntry(cssEntry);
            buffer = new byte[1024];
            for (int count; (count = cssIn.read(buffer, 0, buffer.length)) != -1; ) {
                zipOutputStream.write(buffer, 0, count);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return outputFile;
    }

    private File createCssFile(HttpServletRequest request) {
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        File cssFile = new File(System.currentTimeMillis() + ".css");
        Optional<UserCss> css = userCssRepository.getLastForIp(ipAddress);
        if (css.isPresent()) {
            try (FileWriter writer = new FileWriter(cssFile)) {
                writer.write(css.get().getCss());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return cssFile;
    }

    @Override
    protected String getFileExtension() {
        return ".zip";
    }

}
