package pl.koobe24.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.koobe24.bean.UserCss;
import pl.koobe24.form.UserCssForm;
import pl.koobe24.repository.UserCssRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Created by tomasz on 27.12.16.
 */
@Controller
@RequestMapping("/xml_style")
public class XmlStyleController {

    private UserCssRepository userCssRepository;

    @Autowired
    public void setUserCssRepository(UserCssRepository userCssRepository) {
        this.userCssRepository = userCssRepository;
    }

    @RequestMapping("/*")
    public String xmlStyling(Model model, HttpServletRequest request) {
        model.addAttribute("template1", UserCssForm.FIRST_TEMPLATE);
        model.addAttribute("template2", UserCssForm.SECOND_TEMPLATE);
        model.addAttribute("template3", UserCssForm.THIRD_TEMPLATE);
        return "xml_style";
    }

    @PostMapping("/save")
    public String saveCss(@ModelAttribute("user_css") UserCssForm userCssForm, HttpServletRequest request) {
        UserCss userCss = new UserCss();
        userCss.setCss(UserCssForm.formToStringCss(userCssForm));
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        userCss.setIp(ipAddress);
        userCssRepository.save(userCss);
        return "success";
    }
}
