package pl.koobe24.controller.util;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;

/**
 * Created by tomas on 03.12.2016.
 */
public class OgdlCreator {

    @SuppressWarnings("WeakerAccess")
    public static <T>  String objectToOgdlString(T o) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(o.getClass().getSimpleName()).append("\n");
        try {
            Field[] fields = o.getClass().getDeclaredFields();
            PropertyDescriptor[] pds = Introspector.getBeanInfo(o.getClass()).getPropertyDescriptors();
            for (Field field : fields) {
                for (PropertyDescriptor pd : pds) {
                    if (pd.getReadMethod() != null && pd.getName().equals(field.getName())) {
                        stringBuilder.append("\t");
                        if(field.getType().getSimpleName().equals("String")){
                            stringBuilder.append(field.getName()).append(" \"").append(pd.getReadMethod().invoke(o)).append("\"");
                        }
                        else{
                            stringBuilder.append(field.getName()).append(" ").append(pd.getReadMethod().invoke(o));
                        }
                        stringBuilder.append("\n");
                    }
                }
            }
        } catch (IntrospectionException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    public static <T> String collectionToOgdlString(Collection<T> collection) {
        StringBuilder builder = new StringBuilder();
        for (T o : collection) {
            builder.append(objectToOgdlString(o));
            builder.append("\n");
        }
        return builder.toString();
    }

    public static <T> String collectionToOgdlString(T[] collection) {
        return collectionToOgdlString(Arrays.asList(collection));
    }
}