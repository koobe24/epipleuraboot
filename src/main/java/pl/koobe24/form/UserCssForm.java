package pl.koobe24.form;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import pl.koobe24.bean.Song;
import pl.koobe24.bean.UserCss;
import pl.koobe24.form.template.SongClassProperty;

import java.lang.reflect.Field;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by tomas on 28.12.2016.
 */
//todo refaktoryzacja pozniej koniecznie
@Data
@Slf4j
public class UserCssForm {

    public static UserCssForm FIRST_TEMPLATE;
    public static UserCssForm SECOND_TEMPLATE;
    public static UserCssForm THIRD_TEMPLATE;

    private Collection<SongClassProperty> parameters;

    static {
        FIRST_TEMPLATE = new UserCssForm();
        List<SongClassProperty> parameters = (List<SongClassProperty>) FIRST_TEMPLATE.getParameters();
        for (int i = 0; i < parameters.size(); i++) {
            if (i % 2 == 0) {
                parameters.get(i).setBackgroundColor("#E0FFFF");
            } else {
                parameters.get(i).setBackgroundColor("#90EE90");
            }
        }
        parameters.get(0).setBold(true);
        parameters.get(2).setHideElement(true);

        SECOND_TEMPLATE = new UserCssForm();
        List<SongClassProperty> parameters2 = (List<SongClassProperty>) SECOND_TEMPLATE.getParameters();
        for (int i = 0; i < parameters2.size(); i++) {
            if (i % 2 == 0) {
                parameters2.get(i).setBackgroundColor("#2F4F4F");
            } else {
                parameters2.get(i).setBackgroundColor("#696969");
            }
            parameters2.get(i).setTextColor("#FFFFFF");
        }
        parameters2.get(0).setItalics(true);

        THIRD_TEMPLATE = new UserCssForm();
        List<SongClassProperty> parameters3 = (List<SongClassProperty>) THIRD_TEMPLATE.getParameters();
        for (int i = 0; i < parameters3.size(); i++) {
            if (i % 2 == 0) {
                parameters3.get(i).setBackgroundColor("#FFA500");
            } else {
                parameters3.get(i).setBackgroundColor("#B0E0E6");
            }
            parameters3.get(i).setTextColor("#000000");
        }
        parameters3.get(0).setFontSize(30);
        parameters3.get(3).setUnderline(true);
    }

    public UserCssForm() {
        parameters = new ArrayList<>();
        for (Field f : Song.class.getDeclaredFields()) {
            SongClassProperty template = new SongClassProperty();
            template.setParameterName(f.getName());
            parameters.add(template);
        }
    }

    public static String formToStringCss(UserCssForm userCssForm) {
        userCssForm.getParameters().forEach(System.out::println);
        StringBuilder builder = new StringBuilder();
        builder.append("songList{\n" +
                "display:table;\n" +
                "table-layout:fixed;\n" +
                "width:90%;\n" +
                "margin: 0 auto;\n" +
                "}\n");
        builder.append("song{\n" +
                "display: table-row-group;\n" +
                "}\n");
        builder.append("*{word-wrap:break-word;}" +
                "song > *:hover{\n" +
                "background-color: blue;\n" +
                "}\n" +
                "song > *:active{\n" +
                "background-color: red;\n" +
                "}\n");
        userCssForm.getParameters().forEach(template -> builder.append(getHeaderCssForParameter(template)));
        userCssForm.getParameters().forEach(template -> builder.append(getCssForParameter(template)));
        System.out.println(builder.toString());
        return builder.toString();
    }

    //todo moze jakos zgeneralizowac?
    private static String getHeaderCssForParameter(SongClassProperty template) {
        StringBuilder builder = new StringBuilder();
        builder.append("song:first-child ").append(template.getParameterName())
                .append(":before{")
                .append("content: '").append(template.getParameterName()).append("';")
                .append("display:block;\n" +
                        "color: #000000;\n" +
                        "font-size:24px;\n" +
                        "font-weight: bold;\n" +
                        "width: 100%;\n" +
                        "background-color: grey;\n}");
        return builder.toString();
    }

    private static String getCssForParameter(SongClassProperty template) {
        StringBuilder builder = new StringBuilder();
        builder.append(template.getParameterName()).append("{\ndisplay:table-cell;\n");
        if (template.getBackgroundColor() != null && !template.getBackgroundColor().isEmpty()) {
            builder.append("background-color: ").append(template.getBackgroundColor()).append(";\n");
        }
        if (template.getTextColor() != null && !template.getTextColor().isEmpty()) {
            builder.append("color: ").append(template.getTextColor()).append(";\n");
        }
        if (template.isBold()) {
            builder.append("font-weight: bold;\n");
        }
        if (template.isItalics()) {
            builder.append("font-style: italic;\n");
        }
        if (template.isUnderline()) {
            builder.append("font-decoration: underline;\n");
        }
        if (template.getLetterSpacing() != 0) {
            builder.append("letter-spacing: ").append(template.getLetterSpacing()).append("px;\n");
        }
        if (template.getLineHeight() != 0) {
            builder.append("line-height: ").append(template.getLineHeight()).append(";\n");
        }
        if (template.getFontSize() != 0) {
            builder.append("font-size: ").append(template.getFontSize()).append("px;\n");
        }
        if (template.isHideElement()) {
            builder.append("display: none;\n");
        }
        if (template.getMargin() != 0) {
            builder.append("margin: ").append(template.getMargin()).append("px;\n");
        }
        if (template.getPadding() != 0) {
            builder.append("padding: ").append(template.getPadding()).append("px;\n");
        }
        builder.append("}\n");
        return builder.toString();
    }

    public static UserCssForm fromUserCss(UserCss userCss) {
        List<SongClassProperty> parameters = new ArrayList<>();
        String[] lines = userCss.getCss().split("\\n");
        SongClassProperty currentProperty = new SongClassProperty();
        for (String line : lines) {
            if (line.endsWith("{")) {
                currentProperty = createPropertyForName(line);
            } else if (line.startsWith("*")) {
                //todo potem klasa
            } else if (line.endsWith("}")) {
                parameters.add(currentProperty);
                log.info(currentProperty.toString());
            } else {
                addParameterToProperty(currentProperty, line);
            }
        }
        UserCssForm userCssForm = new UserCssForm();
        userCssForm.setParameters(parameters);
        return userCssForm;
    }

    private static SongClassProperty createPropertyForName(String line) {
        SongClassProperty prop = new SongClassProperty();
        prop.setParameterName(line.substring(0, line.indexOf("{")));
        log.info("property name: " + prop.getParameterName());
        return prop;
    }

    private static void addParameterToProperty(SongClassProperty currentProperty, String line) {
        if (line.contains("background-color: ")) {
            String substring = line.substring(line.indexOf("#"), line.indexOf(";"));
            currentProperty.setBackgroundColor(substring);
        } else if (line.contains("color: ")) {
            String substring = line.substring(line.indexOf("#"), line.indexOf(";"));
            currentProperty.setTextColor(substring);
        } else if (line.contains("font-weight: bold")) {
            currentProperty.setBold(true);
        } else if (line.contains("font-style: italic")) {
            currentProperty.setItalics(true);
        } else if (line.contains("font-decoration: underline")) {
            currentProperty.setUnderline(true);
        } else if (line.contains("letter-spacing: ")) {
            String substring = line.substring(line.indexOf(" ") + 1, line.lastIndexOf("p"));
            currentProperty.setLetterSpacing(Integer.parseInt(substring));
        } else if (line.contains("line-height: ")) {
            String substring = line.substring(line.indexOf(" ") + 1, line.lastIndexOf(";"));
            currentProperty.setLineHeight(Integer.parseInt(substring));
        } else if (line.contains("font-size: ")) {
            String substring = line.substring(line.indexOf(" ") + 1, line.lastIndexOf("p"));
            currentProperty.setFontSize(Integer.parseInt(substring));
        } else if (line.contains("display: none")) {
            currentProperty.setHideElement(true);
        } else if (line.contains("margin: ")) {
            String substring = line.substring(line.indexOf(" ") + 1, line.lastIndexOf("p"));
            currentProperty.setMargin(Integer.parseInt(substring));
        } else if (line.contains("padding: ")) {
            String substring = line.substring(line.indexOf(" ") + 1, line.lastIndexOf("p"));
            currentProperty.setPadding(Integer.parseInt(substring));
        }
    }

}
