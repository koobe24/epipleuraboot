package pl.koobe24.form.template;

import lombok.Data;

/**
 * Created by tomas on 28.12.2016.
 */
@Data
public class SongClassProperty {
    private String parameterName;
    private String textColor;
    private String backgroundColor;
    private boolean italics;
    private boolean underline;
    private boolean bold;
    private int letterSpacing;
    private double lineHeight;
    private int fontSize;
    private boolean hideElement;
    private int margin;
    private int padding;

    public SongClassProperty() {
        backgroundColor = "#FFFFFF";
    }

}
