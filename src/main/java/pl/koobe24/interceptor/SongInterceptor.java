package pl.koobe24.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import pl.koobe24.bean.UserData;
import pl.koobe24.exception.ForbiddenException;
import pl.koobe24.repository.UserDataRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by tomasz on 24.12.16.
 */
class SongInterceptor extends HandlerInterceptorAdapter {
    private static final int MAXIMUM_QUERIES = 100;
    private static final int TIMEOUT_MILLISECONDS = 300000;
    private UserDataRepository userDataRepository;
    private HashMap<String, Date> map = new HashMap<>();

    @Autowired
    public void setUserDataRepository(UserDataRepository userDataRepository) {
        this.userDataRepository = userDataRepository;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        if (map.containsKey(ipAddress)) {
            if (map.get(ipAddress).after(new Date())) {
                throw new ForbiddenException();
            } else {
                map.remove(ipAddress);
            }
        } else if (userDataRepository.countLastQueries(ipAddress, new Date()) > MAXIMUM_QUERIES) {
            map.put(ipAddress, new Date(System.currentTimeMillis() + TIMEOUT_MILLISECONDS));
            throw new ForbiddenException();
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        UserData userData = new UserData();
        userData.setView(handler.toString());
        userData.setUserAgent(request.getHeader("User-Agent"));
        userData.setDateTime(new Date());
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        userData.setIpAddress(ipAddress);
        userDataRepository.save(userData);
    }
}
