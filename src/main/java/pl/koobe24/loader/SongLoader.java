package pl.koobe24.loader;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import pl.koobe24.bean.Song;
import pl.koobe24.loader.util.SongCreator;
import pl.koobe24.repository.SongRepository;

import java.util.Collection;

/**
 * Created by tomasz on 25.12.16.
 */
@Component
@Slf4j
public class SongLoader implements ApplicationListener<ContextRefreshedEvent> {
    private final static int NUMBER_OF_RECORDS = 500;
    private final static int SEED = 1;
    private SongRepository songRepository;

    @Autowired
    void setSongRepository(SongRepository songRepository) {
        this.songRepository = songRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        if (!songRepository.exists(1)) {
            log.info("Adding sample songs.");
            SongCreator creator = new SongCreator(NUMBER_OF_RECORDS, SEED);
            Collection<Song> songsToAdd = creator.createSongs();
            songRepository.save(songsToAdd);
        } else {
            log.info("Sample data not needed.");
        }
    }
}
