package pl.koobe24.loader.util;

import pl.koobe24.bean.Song;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

/**
 * Created by tomasz on 25.12.16.
 */
public class SongCreator {

    private final int BPM_START_LEVEL = 80;
    private final int BPM_DIFFERENCE_MAX = 50;
    private final String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    private final int COMMENT_LENGTH = 20;
    private final int DURATION_SECONDS_START = 150;
    private final int DURATION_DIFFERENCE_MAX = 300;
    private final int MAXIMUM_SEGMENT_LOUDNESS_START_LEVEL = 20;
    private final int MAXIMUM_SEGMENT_LOUDNESS_DIFFERECE_MAX = 50;
    private final int OVERALL_LOUDNESS_DIFFERENCE = 10;
    private final int NUMBER_OF_TAGS = 3;
    private final int BASE_YEAR = 1980;
    private final int YEAR_DIFFERENCE_MAX = 35;

    private final String[][] songsTitles = {
            {"kocham", "lubie", "nienawidze", "patrze", "widze", "ide", "biegne", "spiewam", "jestem", "pytam"},
            {"na", "w", "za", "pod"},
            {"stole", "scianie", "dworze", "Juracie"},
            {"bo", "gdyz", "i"},
            {"o", "tak", "nie", "lubie"}
    };

    private final String[] publisher = {"Beggars Music", "Bicycle Music Company", "Carlin", "Curb Music", "Imagem Music", "hearts Bluff Music", "Warner-Chappell Music", "Word Music Publishing", "RAK Music", "Reservoir"};
    private final String[] artists = {"Metallica", "OSTR", "Taco", "Led Zeppelin", "Michael Jackson"};
    private final String[] albums = {"Metallica", "Tabasko", "Young hems", "Led Zeppelin X", "Michael Jackson revived"};
    private final int[] sampleRate = {37800, 44100, 47250, 48000, 50000, 50400, 88200, 96000};
    private final String[] tags = {"YOLO", "bestsong", "bumpin", "repeat", "listentothis", "godmusic", "instamusic", "myjam", "love"};

    private int howMany;
    private long seed;
    private Random generator;

    public SongCreator(int howMany) {
        this.howMany = howMany;
        generator = new Random(System.currentTimeMillis());
    }

    public SongCreator(int howMany, int seed) {
        this.howMany = howMany;
        generator = new Random(seed);
    }

    public Collection<Song> createSongs() {
        Collection<Song> songs = new ArrayList<>();
        for (int i = 0; i < howMany; i++) {
            int artistAndAlbumId = getArtistAndAlbumId();
            Song song = Song.builder().BPM(generateBPM())
                    .artist(artists[artistAndAlbumId])
                    .album(albums[artistAndAlbumId])
                    .comments(generateRandomStringForComment())
                    .duration(generateDuration())
                    .feat(generateFeatArtist())
                    .genre(generateGenre())
                    .maximumSegmentLoudness(generateMaximumSegmentLoudness())
                    .overallLoudness(generateMaximumSegmentLoudness() - OVERALL_LOUDNESS_DIFFERENCE)
                    .publisher(generatePublisher())
                    .sampleRate(generateSampleRate())
                    .tags(generateTags())
                    .year(generateYear())
                    .title(generateTitle()).build();

            songs.add(song);
        }
        return songs;
    }

    private int generateBPM() {
        return (BPM_START_LEVEL + generator.nextInt(BPM_DIFFERENCE_MAX));
    }

    private int getArtistAndAlbumId() {
        return generator.nextInt(artists.length - 1);
    }

    private String generateRandomStringForComment() {
        StringBuilder comment = new StringBuilder();
        while (comment.length() < COMMENT_LENGTH) {
            int charIndex = (int) (generator.nextFloat() * SALTCHARS.length());
            comment.append(SALTCHARS.charAt(charIndex));
        }
        return comment.toString();
    }

    private int generateDuration() {
        return (DURATION_SECONDS_START + generator.nextInt(DURATION_DIFFERENCE_MAX));
    }

    private String generateFeatArtist() {
        return artists[generator.nextInt(artists.length - 1)];
    }

    private Song.Genre generateGenre() {
        Song.Genre[] genres = Song.Genre.values();
        return genres[generator.nextInt(genres.length - 1)];
    }

    private int generateMaximumSegmentLoudness() {
        return MAXIMUM_SEGMENT_LOUDNESS_START_LEVEL + generator.nextInt(MAXIMUM_SEGMENT_LOUDNESS_DIFFERECE_MAX);
    }

    private String generatePublisher() {
        return publisher[generator.nextInt(publisher.length - 1)];
    }

    private int generateSampleRate() {
        return sampleRate[generator.nextInt(sampleRate.length - 1)];
    }

    private String generateTags() {
        StringBuilder bu = new StringBuilder();
        for (int j = 0; j < NUMBER_OF_TAGS; j++) {
            bu.append(tags[generator.nextInt(tags.length - 1)]);
            bu.append(" ");
        }
        return bu.toString();
    }

    private int generateYear() {
        return BASE_YEAR + generator.nextInt(YEAR_DIFFERENCE_MAX);
    }

    private String generateTitle() {
        StringBuilder songTitleBuilder = new StringBuilder();
        songTitleBuilder.append(songsTitles[0][generator.nextInt(songsTitles[0].length - 1)])
                .append(" ")
                .append(songsTitles[1][generator.nextInt(songsTitles[1].length - 1)])
                .append(" ")
                .append(songsTitles[2][generator.nextInt(songsTitles[2].length - 1)])
                .append(" ")
                .append(songsTitles[3][generator.nextInt(songsTitles[3].length - 1)])
                .append(" ")
                .append(songsTitles[4][generator.nextInt(songsTitles[4].length - 1)]);
        return songTitleBuilder.toString();
    }
}
