package pl.koobe24.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.koobe24.bean.Song;

import java.util.Collection;

/**
 * Created by tomasz on 24.12.16.
 */
@Repository
public interface SongRepository extends CrudRepository<Song, Integer> {
    @Query(name = "getTenWithIndex", nativeQuery = true)
    Collection<Song> getTenWithIndex(@Param("index") int index);

    @Query(name = "getBySongNameStartingWith", nativeQuery = true)
    Collection<Song> getBySongNameStartingWith(@Param("name") String name);

    @Query(name = "getOrderedByOverallLoudness", nativeQuery = true)
    Collection<Song> getOrderedByOverallLoudness(@Param("ascending") int ascending);

    @Query(name = "getWithMinimalAndMaximalDuration", nativeQuery = true)
    Collection<Song> getWithMinimalAndMaximalDuration(@Param("min") int min, @Param("max") int max);

    @Query(name = "getByAlbumSortedByDuration", nativeQuery = true)
    Collection<Song> getByAlbumSortedByDuration(@Param("albumName") String albumName, @Param("ascending") int ascending);

    @Query(name = "getSongsWithArtistAndPublisher", nativeQuery = true)
    Collection<Song> getSongsWithArtistAndPublisher(@Param("p_artist") String artist, @Param("p_publisher") String publisher);

    @Query(name = "getFromEvenOrUnevenYears", nativeQuery = true)
    Collection<Song> getFromEvenOrUnevenYears(@Param("even") boolean even);

    @Query(name = "getLongestAndShortestSongs", nativeQuery = true)
    Collection<Song> getLongestAndShortestSongs(@Param("howMany") int howMany);

    @Query(name = "getSongsWithTag", nativeQuery = true)
    Collection<Song> getSongsWithTag(@Param("tag") String tag);

    @Query(name = "getRandomSongsFromArtist", nativeQuery = true)
    Collection<Song> getRandomSongsFromArtist(@Param("howMany") int howMany, @Param("p_artist") String artist);

    @Query("SELECT distinct s.artist from Song s")
    Collection<String> getArtists();

    @Query("SELECT distinct s.album from Song s")
    Collection<String> getAlbums();

    @Query("SELECT distinct s.publisher from Song s")
    Collection<String> getPublishers();
}
