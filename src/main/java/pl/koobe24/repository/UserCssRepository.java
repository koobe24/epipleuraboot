package pl.koobe24.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.koobe24.bean.UserCss;

import java.util.Optional;

/**
 * Created by tomas on 28.12.2016.
 */
@Repository
public interface UserCssRepository extends CrudRepository<UserCss, Integer> {

    @Query(name = "getLastCssForIp", nativeQuery = true)
    Optional<UserCss> getLastForIp(@Param("p_ip") String ip);
}
