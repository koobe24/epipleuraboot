package pl.koobe24.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import pl.koobe24.bean.UserData;

import java.util.Date;

/**
 * Created by tomasz on 24.12.16.
 */
public interface UserDataRepository extends CrudRepository<UserData, Integer> {
    @Query("SELECT count(*) from user_data u where u.ipAddress = :ip and u.dateTime > :date")
    long countLastQueries(@Param("ip") String ip, @Param("date") Date date);
}
