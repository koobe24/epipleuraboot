package pl.koobe24.pl.koobe24.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import pl.koobe24.controller.MainController;
import pl.koobe24.repository.SongRepository;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by tomas on 30.12.2016.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(MainController.class)
public class MainControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private SongRepository songRepository;

    @Test
    public void mainShouldContainListOfArtistPublishersAlbums() throws Exception {
        String album1 = "a1";
        String album2 = "a2";
        String publisher1 = "p1";
        String publisher2 = "p2";
        String artist1 = "ar1";
        String artist2 = "ar2";

        when(songRepository.getAlbums()).thenReturn(Arrays.asList(album1, album2));
        when(songRepository.getPublishers()).thenReturn(Arrays.asList(publisher1, publisher2));
        when(songRepository.getArtists()).thenReturn(Arrays.asList(artist1, artist2));

        mvc.perform(get("/")).andExpect(status().isOk())
                .andExpect(model().attribute("artists", hasSize(2)))
                .andExpect(model().attribute("artists", hasItem(artist1)))
                .andExpect(model().attribute("artists", hasItem(artist2)))
                .andExpect(model().attribute("publishers", hasSize(2)))
                .andExpect(model().attribute("publishers", hasItem(publisher1)))
                .andExpect(model().attribute("publishers", hasItem(publisher2)))
                .andExpect(model().attribute("albums", hasSize(2)))
                .andExpect(model().attribute("albums", hasItem(album1)))
                .andExpect(model().attribute("albums", hasItem(album2)));

        verify(songRepository, times(1)).getAlbums();
        verify(songRepository, times(1)).getArtists();
        verify(songRepository, times(1)).getPublishers();
        verifyNoMoreInteractions(songRepository);
    }

}
