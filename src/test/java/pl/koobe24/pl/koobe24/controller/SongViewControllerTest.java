package pl.koobe24.pl.koobe24.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import pl.koobe24.bean.Song;
import pl.koobe24.controller.MainController;
import pl.koobe24.controller.SongViewController;
import pl.koobe24.repository.SongRepository;

import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by tomas on 30.12.2016.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(SongViewController.class)
public class SongViewControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private SongRepository songRepository;

    @Test
    public void showSongsByNameHasListAndNameParameter() throws Exception {
        Song s1 = Song.builder().id(1).build();
        Song s2 = Song.builder().id(2).build();
        String testName = "test";

        when(songRepository.getBySongNameStartingWith(testName)).thenReturn(Arrays.asList(s1, s2));

        mvc.perform(get("/view/showSongsByName?name=" + testName)).andExpect(status().isOk())
                .andExpect(model().attribute("songs", hasSize(2)))
                .andExpect(model().attribute("songs", hasItem(s1)))
                .andExpect(model().attribute("songs", hasItem(s2)))
                .andExpect(model().attribute("name", not(isEmptyString())))
                .andExpect(model().attribute("name", is(testName)));

        verify(songRepository, times(1)).getBySongNameStartingWith(testName);
        verifyNoMoreInteractions(songRepository);
    }

    @Test
    public void getTenWIthIndexHasListOfSongsAndIndexParameter() throws Exception {
        Song s1 = Song.builder().id(1).build();
        Song s2 = Song.builder().id(2).build();
        int index = 1;

        when(songRepository.getTenWithIndex(index)).thenReturn(Arrays.asList(s1, s2));

        mvc.perform(get("/view/getTenWithIndex?index=" + index)).andExpect(status().isOk())
                .andExpect(model().attribute("songs", hasSize(2)))
                .andExpect(model().attribute("songs", hasItem(s1)))
                .andExpect(model().attribute("songs", hasItem(s2)))
                .andExpect(model().attribute("index", is(index)));

        verify(songRepository, times(1)).getTenWithIndex(index);
        verifyNoMoreInteractions(songRepository);
    }

    @Test
    public void getOrderedByOverallLoudnessHasListOfSongsAndAscendingParameter() throws Exception {
        Song s1 = Song.builder().id(1).build();
        Song s2 = Song.builder().id(2).build();
        String asc = "asc";
        int ascInt = 1;
        when(songRepository.getOrderedByOverallLoudness(ascInt)).thenReturn(Arrays.asList(s1, s2));

        mvc.perform(get("/view/getOrderedByOverallLoudness?ascending=" + asc)).andExpect(status().isOk())
                .andExpect(model().attribute("songs", hasSize(2)))
                .andExpect(model().attribute("songs", hasItem(s1)))
                .andExpect(model().attribute("songs", hasItem(s2)))
                .andExpect(model().attribute("ascending", not(isEmptyString())))
                .andExpect(model().attribute("ascending", is(asc)));

        verify(songRepository, times(1)).getOrderedByOverallLoudness(ascInt);
        verifyNoMoreInteractions(songRepository);
    }

    @Test
    public void getByAlbumSortedByDurationHasListOfSongsAndParameters() throws Exception {
        Song s1 = Song.builder().id(1).build();
        Song s2 = Song.builder().id(2).build();
        String asc = "asc";
        int ascInt = 1;
        String name = "test";
        when(songRepository.getByAlbumSortedByDuration(name, ascInt)).thenReturn(Arrays.asList(s1, s2));

        mvc.perform(get("/view/getByAlbumSortedByDuration?name=" + name + "&ascending=" + asc)).andExpect(status().isOk())
                .andExpect(model().attribute("songs", hasSize(2)))
                .andExpect(model().attribute("songs", hasItem(s1)))
                .andExpect(model().attribute("songs", hasItem(s2)))
                .andExpect(model().attribute("ascending", not(isEmptyString())))
                .andExpect(model().attribute("ascending", is(asc)))
                .andExpect(model().attribute("name", not(isEmptyString())))
                .andExpect(model().attribute("name", is(name)));

        verify(songRepository, times(1)).getByAlbumSortedByDuration(name, ascInt);
        verifyNoMoreInteractions(songRepository);
    }

}
